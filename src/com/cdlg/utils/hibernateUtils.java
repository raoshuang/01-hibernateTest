package com.cdlg.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class hibernateUtils {
	private static SessionFactory sessionFactory;

	public static Session getSession() {		
		return getSessionFactory().getCurrentSession();		
	}

	public static SessionFactory getSessionFactory() {
		//由于SessionFactory()是重量级的,因此应该做成单例,节省开销
		if (sessionFactory == null || sessionFactory.isClosed()) {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		}
		return sessionFactory;
	}
}
