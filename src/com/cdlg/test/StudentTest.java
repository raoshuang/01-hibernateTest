package com.cdlg.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import com.cdlg.beans.Student;

/**
 * 不用工具类进行创建session进行业务操作。
 * */
public class StudentTest {
	
	@Test
	public void testSave() {
		//1.加载主配置文件：hibernate.cfg.xml
		Configuration configure = new Configuration().configure();
		//2.创建Session工厂对象
		SessionFactory sessionFactory = configure.buildSessionFactory();
		//3.获取Session对象
		Session session = sessionFactory.getCurrentSession();
		try {
			//4.开启事务
			session.beginTransaction();
			//session .getTransaction().begin();//此代码功能等同上句
			//5.执行操作
			Student student = new Student("张三",23,94.5);
			session.save(student);
			//6.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//7.事务回滚
			session.getTransaction().rollback();
		}
	}
	/**
	 * 在hibernate中主配置文件是可以改的,但是没有必要。
	 * */
	@Test
	public void testSave2() {
		//1.加载主配置文件：xxx.cfg.xml
		Configuration configure = new Configuration().configure("xxx.cfg.xml");
		//2.创建Session工厂对象
		SessionFactory sessionFactory = configure.buildSessionFactory();
		//3.获取Session对象
		Session session = sessionFactory.getCurrentSession();
		try {
			//4.开启事务
			session.beginTransaction();
			//5.执行操作
//			Student student = new Student("张三",23,94.5);
			Student student = new Student("李四",22,92.5);
			session.save(student);
			//6.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//7.事务回滚
			session.getTransaction().rollback();
		}
	}
	/**
	 * 通过sessionFactory.openSession()方法获得两个session是不同的，这就不能回话
	 * java的四大回话技术：1.cookie,2.session,3.隐藏表单域,4.URL重写
	 * */
	@Test
	public void testSave3() {
		//1.加载主配置文件：xxx.cfg.xml
		Configuration configure = new Configuration().configure("xxx.cfg.xml");
		//2.创建Session工厂对象
		SessionFactory sessionFactory = configure.buildSessionFactory();
		//3.获取Session对象
		Session session1 = sessionFactory.openSession();
		Session session2 = sessionFactory.openSession();
		
		System.out.println(session1 == session2);//false
	}
	/**
	 * 通过sessionFactory.getCurrentSession()方法获得session是同一个,这就可实现回话了.
	 * */
	@Test
	public void testSave4() {
		//1.加载主配置文件：xxx.cfg.xml
		Configuration configure = new Configuration().configure("xxx.cfg.xml");
		//2.创建Session工厂对象
		SessionFactory sessionFactory = configure.buildSessionFactory();
		//3.获取Session对象
		Session session1 = sessionFactory.getCurrentSession();
		Session session2 = sessionFactory.getCurrentSession();
		
		System.out.println(session1 == session2);//
	}
}
