package com.cdlg.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import com.cdlg.beans.Student;
import com.cdlg.utils.hibernateUtils;

/**
 * 使用工具类进行创建session进行业务操作。
 * */
public class StudentTest2 {
	
	@Test
	public void testSave() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			Student student = new Student("张三",23,94.5);
			session.save(student);
//			session.persist(student);
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
	@Test
	public void testDelete() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			Student student = new Student();
			student.setId(1);
			//删除条件是对象要具有Id
			session.delete(student);
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
	@Test
	public void testUpdate() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			Student student = new Student("李四",24,110.5);
			student.setId(2);
			//更改条件是对象要具有Id
			session.update(student);
			
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
	/**主键查询方法名有：get()和load()
	 * get()和load()的共同点:根据id加载对象
	 * get()和load()的区别:前者若加载的对象不存在,则返回null;
	 * 					后者前者若加载的对象不存在,则抛出异常.
	 **/
	@Test
	public void testGet() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			Student student = session.get(Student.class,2);
			System.out.println(student);
			
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
	/**
	 * SaveOrUpdate()方法,当操作的对象有设置id时,执行的事务就是update,当操作的对象没有设置id时,执行的就是save.
	 */
	@Test
	public void testSaveOrUpdate() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			Student student = new Student("李小四",24,99.5);
			student.setId(2);
			//当对象有id时,执行的操作是update,不管数据库中有没有。
			session.saveOrUpdate(student);			
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
	@Test
	public void testGetLoad() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		//通过getCurrentSession()获取的session所执行的查询操作，必须在事务环境下运行。
		Student student = session.get(Student.class,2);
		System.out.println(student);//junit会抛出异常，org.hibernate.HibernateException: get is not valid without active transaction
	}
	@Test
	public void testGetLoad2() {
		//获取session对象
		Session session = hibernateUtils.getSessionFactory().openSession();
		//通过openSession()获取的session所执行的查询操作，无需一定要在事务环境下运行。
		Student student = session.get(Student.class,2);
		System.out.println(student);//Student [id=2, name=李小四, age=24, score=99.5]
	}
	@Test
	public void testCURD() {
		//获取session对象
		Session session = hibernateUtils.getSession();
		try {
			//2.开启事务
			session.beginTransaction();
			//3.执行操作
			//增删改操作若是放在同一事务下运行，则系统默认执行的顺序是:增改删
			//删除
			Student student = session.get(Student.class, 2);			
			session.delete(student);
			//使用session.flush()方法刷新session缓存后,事务的执行顺序为代码的先后顺序.
			session.flush();
			//插入
			Student student1 = new Student("王五",25,120.2);
			session.save(student1);
			//修改
			Student student2 = session.get(Student.class, 3);
			student2.setName("赵六");
			session.save(student2);
			//4.事务提交
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			//4.事务回滚
			session.getTransaction().rollback();
		}
	}
}
